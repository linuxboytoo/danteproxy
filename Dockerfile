FROM alpine:edge

RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing">>/etc/apk/repositories
RUN apk update && apk upgrade && apk add iptables dante-server@testing

ADD sockd.conf /etc/sockd.conf
ADD start.sh /start.sh
RUN chmod +x /start.sh

ENTRYPOINT ["/start.sh"]
