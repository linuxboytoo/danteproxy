#!/bin/sh

WHITELISTIP=$1
if [ -z $WHITELISTIP ]; then
  echo "No Whitelist IP Specified"
  exit 1
fi

iptables -F
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP

iptables -A INPUT --src $WHITELISTIP -p tcp --dport 2222 -j ACCEPT
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

while true
do
  sockd
  echo "####### PROCESS EXITED - RESTARTING..."
done
